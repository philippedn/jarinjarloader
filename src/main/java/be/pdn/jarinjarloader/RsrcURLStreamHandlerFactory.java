/**
 * JarInJarLoader
 * Copyright (C) 2021
 *
 * This program and the accompanying materials are made available under the terms of the
 * Eclipse Public License v2.0 which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-v20.html
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package be.pdn.jarinjarloader;

import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;

public class RsrcURLStreamHandlerFactory implements URLStreamHandlerFactory {

  private ClassLoader classLoader;

  public RsrcURLStreamHandlerFactory(ClassLoader cl) {
    this.classLoader = cl;
  }

  @Override
  public URLStreamHandler createURLStreamHandler(String protocol) {
    if (protocol.equals("rsrc"))
      return new RsrcURLStreamHandler(classLoader);
    return null;
  }

}
