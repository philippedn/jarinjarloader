/**
 * JarInJarLoader
 * Copyright (C) 2021
 *
 * This program and the accompanying materials are made available under the terms of the
 * Eclipse Public License v2.0 which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-v20.html
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package be.pdn.jarinjarloader;

import static java.util.Arrays.stream;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

public class JarInJarLoader {

  public static void main(String[] args) throws Exception {
    var manifestInfo = ManifestInfo.build();
    var contextClassLoader = Thread.currentThread().getContextClassLoader();
    var rsrcURLStreamHandlerFactory = new RsrcURLStreamHandlerFactory(contextClassLoader);
    URL.setURLStreamHandlerFactory(rsrcURLStreamHandlerFactory);
    var rsrcPathURLs = stream(manifestInfo.rsrcPaths())
        .map(JarInJarLoader::toURL)
        .toArray(length -> new URL[length]);
    var platformClassLoader = ClassLoader.getPlatformClassLoader();
    var rsrcClassLoader = new URLClassLoader(rsrcPathURLs, platformClassLoader);
    Thread.currentThread().setContextClassLoader(rsrcClassLoader);
    var mainClass = Class.forName(manifestInfo.rsrcMainClass(), true, rsrcClassLoader);
    var mainMethod = mainClass.getMethod("main", String[].class);
    mainMethod.invoke(null, new Object[] { args });
  }

  private static URL toURL(String rsrcPath) {
    try {
      if (rsrcPath.endsWith("/"))
        return new URL("rsrc:" + rsrcPath);
      else
        return new URL("jar:rsrc:" + rsrcPath + "!/");
    }
    catch (MalformedURLException e) {
      throw new RuntimeException(e);
    }
  }

  private static record ManifestInfo(String rsrcMainClass, String[] rsrcPaths) {
    static ManifestInfo build() throws IOException {
      var manifestInputStream = Thread.currentThread().getContextClassLoader()
          .getResourceAsStream(JarFile.MANIFEST_NAME);
      var manifest = new Manifest(manifestInputStream);
      var manifestEntries = manifest.getMainAttributes();
      var rsrcMainClass = manifestEntries.getValue("Rsrc-Main-Class");
      var rsrcClassPath = manifestEntries.getValue("Class-Path");
      if (rsrcClassPath == null)
        rsrcClassPath = "";
      if (rsrcMainClass != null && !rsrcMainClass.isBlank())
        return new ManifestInfo(rsrcMainClass, rsrcClassPath.split(" "));
      throw new RuntimeException("Missing or empty Rsrc-Main-Class attribute in %s".formatted(JarFile.MANIFEST_NAME));
    }
  };

}
