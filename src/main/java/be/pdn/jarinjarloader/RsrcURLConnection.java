/**
 * JarInJarLoader
 * Copyright (C) 2021
 *
 * This program and the accompanying materials are made available under the terms of the
 * Eclipse Public License v2.0 which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-v20.html
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package be.pdn.jarinjarloader;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;

public class RsrcURLConnection extends URLConnection {

  private ClassLoader classLoader;

  public RsrcURLConnection(URL url, ClassLoader classLoader) {
    super(url);
    this.classLoader = classLoader;
  }

  @Override
  public void connect() throws IOException {
  }

  @Override
  public InputStream getInputStream() throws IOException {
    String file = URLDecoder.decode(url.getFile(), "UTF-8");
    InputStream result = classLoader.getResourceAsStream(file);
    if (result == null) {
      throw new MalformedURLException("Could not open InputStream for URL '" + url + "'");
    }
    return result;
  }
}
