/**
 * JarInJarLoader
 * Copyright (C) 2021
 *
 * This program and the accompanying materials are made available under the terms of the
 * Eclipse Public License v2.0 which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-v20.html
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package be.pdn.jarinjarloader;

import java.io.IOException;
import java.net.URL;

/**
 * Handle URLs with protocol "rsrc". "rsrc:path/file.ext" identifies the content accessible as
 * classLoader.getResourceAsStream("path/file.ext"). "rsrc:path/" identifies a base-path for
 * resources to be searched. The spec "file.ext" is combined to "rsrc:path/file.ext".
 */
public class RsrcURLStreamHandler extends java.net.URLStreamHandler {

  private ClassLoader classLoader;

  public RsrcURLStreamHandler(ClassLoader classLoader) {
    this.classLoader = classLoader;
  }

  @Override
  protected java.net.URLConnection openConnection(URL u) throws IOException {
    return new RsrcURLConnection(u, classLoader);
  }

  @Override
  protected void parseURL(URL url, String spec, int start, int limit) {
    String path;
    if (spec.startsWith("rsrc:"))
      path = spec.substring(5);
    else if (url.getFile().equals("./"))
      path = spec;
    else if (url.getFile().endsWith("/"))
      path = url.getFile() + spec;
    else if (spec.equals("#runtime"))
      path = url.getFile();
    else
      path = spec;
    setURL(url, "rsrc", "", -1, null, null, path, null, null);
  }
}
