# JarInJarLoader

## Description
When running a jar with `java -jar`, you still need to add dependencies on the classpath. With JarInJarLoader, you can add
these dependencies in your jar and JarInJarLoader will make these dependencies available to your code, making it unnecessary
to add these dependencies to the classpath.

## Configuration
Configure the `maven-dependency-plugin` and the `maven-jar-plugin` as follows. `Rsrc-Main-Class` still needs to be set to
your main class and `${jarinjarloader.version}` must be replaced with the version of JarInJarLoader. Optionally, add the
configuration to create an executable jar that can be executed as is, instead of with `java -jar` (bash needed).

      <build>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-dependency-plugin</artifactId>
            <executions>
              <execution>
                <id>copy-dependencies</id>
                <phase>prepare-package</phase>
                <goals>
                  <goal>copy-dependencies</goal>
                </goals>
                <configuration>
                  <outputDirectory>${project.build.directory}/classes/lib</outputDirectory>
                  <includeScope>runtime</includeScope>
                </configuration>
              </execution>
              <execution>
                <id>unpack</id>
                <phase>prepare-package</phase>
                <goals>
                  <goal>unpack</goal>
                </goals>
                <configuration>
                  <artifactItems>
                    <artifactItem>
                      <groupId>be.pdn</groupId>
                      <artifactId>jarinjarloader</artifactId>
                      <version>${jarinjarloader.version}</version>
                      <type>jar</type>
                      <overWrite>false</overWrite>
                      <outputDirectory>${project.build.directory}/classes</outputDirectory>
                    </artifactItem>
                  </artifactItems>
                </configuration>
              </execution>
            </executions>
          </plugin>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-jar-plugin</artifactId>
            <configuration>
              <archive>
                <manifest>
                  <addClasspath>true</addClasspath>
                  <classpathPrefix>lib/</classpathPrefix>
                  <mainClass>be.pdn.jarinjarloader.JarInJarLoader</mainClass>
                </manifest>
                <manifestEntries>
                  <Class-Path>./</Class-Path>
                  <Rsrc-Main-Class>main.App</Rsrc-Main-Class>
                </manifestEntries>
              </archive>
            </configuration>
          </plugin>
          <!-- Next plugin configuration only needed if you want to be able to execute the jar without java -jar -->
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>exec-maven-plugin</artifactId>
            <executions>
              <execution>
                <id>move jar</id>
                <phase>package</phase>
                <goals>
                  <goal>exec</goal>
                </goals>
                <configuration>
                  <executable>mv</executable>
                  <commandlineArgs>${project.build.directory}/${project.build.finalName}.jar ${project.build.directory}/${project.build.finalName}.jar.orig</commandlineArgs>
                </configuration>
              </execution>
              <execution>
                <id>generate executable</id>
                <phase>package</phase>
                <goals>
                  <goal>exec</goal>
                </goals>
                <configuration>
                  <executable>bash</executable>
                  <commandlineArgs>-c "cat &lt;(echo -e \#!/bin/bash\\njava -jar \\042\$PWD/\$0\\042\\nexit\\n) ${project.build.directory}/${project.build.finalName}.jar.orig >| ${project.build.directory}/${project.build.finalName}.jar"</commandlineArgs>
                </configuration>
              </execution>
              <execution>
                <id>set executable</id>
                <phase>package</phase>
                <goals>
                  <goal>exec</goal>
                </goals>
                <configuration>
                  <executable>chmod</executable>
                  <commandlineArgs>u+x ${project.build.directory}/${project.build.finalName}.jar</commandlineArgs>
                </configuration>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>

## How it works
A custom classloader will load classes in jars within the main jar specified in the manifest's `Class-Path` entry.

## Credits
Based on [eclipse-jarinjarloader](https://git.eclipse.org/c/jdt/eclipse.jdt.ui.git/tree/org.eclipse.jdt.ui/jar%20in%20jar%20loader/org/eclipse/jdt/internal/jarinjarloader)
